package com.djac21.weatherandnews.models

data class ListItem(
    val dt: Int = 0,
    val pop: Double = 0.0,
    val visibility: Int = 0,
    val dtTxt: String = "",
    val weather: List<WeatherItem>?,
    val main: Main,
    val clouds: Clouds,
    val sys: Sys,
    val wind: Wind
)