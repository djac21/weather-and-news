package com.djac21.weatherandnews.models

data class WeatherItem(
    val icon: String = "",
    val description: String = "",
    val main: String = "",
    val id: Int = 0
)