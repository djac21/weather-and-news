package com.djac21.weatherandnews.models

data class Wind(
    val deg: Int = 0,
    val speed: Double = 0.0
)