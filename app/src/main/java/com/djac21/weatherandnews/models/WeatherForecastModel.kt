package com.djac21.weatherandnews.models

data class WeatherForecastModel(
    val city: City,
    val cnt: Int = 0,
    val cod: String = "",
    val message: Int = 0,
    val list: List<ListItem>?
)