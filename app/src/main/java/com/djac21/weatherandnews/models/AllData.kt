package com.djac21.weatherandnews.models

data class AllData(var weatherModel: WeatherModel,
                   var weatherForecastModel: WeatherForecastModel,
                   var newsModel: NewsModel)