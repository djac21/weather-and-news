package com.djac21.weatherandnews.models

import java.io.Serializable

data class Source(
    val name: String = "",
    val id: String = ""
) : Serializable