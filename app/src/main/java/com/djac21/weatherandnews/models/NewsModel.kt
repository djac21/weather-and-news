package com.djac21.weatherandnews.models

import java.io.Serializable

data class NewsModel(
    val totalResults: Int = 0,
    val articles: List<ArticlesItem>?,
    val status: String = ""
) : Serializable