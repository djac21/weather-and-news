package com.djac21.weatherandnews.api

import android.content.Context
import com.djac21.weatherandnews.models.NewsModel
import com.djac21.weatherandnews.models.WeatherForecastModel
import com.djac21.weatherandnews.models.WeatherModel
import com.djac21.weatherandnews.utils.API_KEY_NEWS
import com.djac21.weatherandnews.utils.API_KEY_WEATHER
import com.djac21.weatherandnews.utils.BASE_URL_NEWS
import com.djac21.weatherandnews.utils.BASE_URL_WEATHER
import io.reactivex.Single

class Repo {
    fun getWeather(context: Context, cityName: String): Single<WeatherModel>? {
        return ApiClient.getRetrofit(context, BASE_URL_WEATHER).create(ApiInterface::class.java).getCurrentWeather(cityName, API_KEY_WEATHER)
            ?.flatMap {
                Single.just(it)
            }
    }

    fun getForecastWeather(context: Context, cityName: String): Single<WeatherForecastModel>? {
        return ApiClient.getRetrofit(context, BASE_URL_WEATHER).create(ApiInterface::class.java).getWeatherForeCast(cityName, API_KEY_WEATHER)
            ?.flatMap {
                Single.just(it)
            }
    }

    fun getNews(context: Context, cityName: String): Single<NewsModel>? {
        return ApiClient.getRetrofit(context, BASE_URL_NEWS).create(ApiInterface::class.java).getNews(cityName, API_KEY_NEWS)
            ?.flatMap {
                Single.just(it)
            }
    }
}