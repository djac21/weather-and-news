package com.djac21.weatherandnews.api

import com.djac21.weatherandnews.models.NewsModel
import com.djac21.weatherandnews.models.WeatherForecastModel
import com.djac21.weatherandnews.models.WeatherModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("weather?&units=imperial")
    fun getCurrentWeather(@Query("q") cityName: String?, @Query("appid") apikey: String?): Single<WeatherModel>?

    @GET("forecast?&units=imperial&cnt=25")
    fun getWeatherForeCast(@Query("q") cityName: String?, @Query("appid") apikey: String?): Single<WeatherForecastModel>?

    @GET("everything")
    fun getNews(@Query("q") category: String?, @Query("apiKey") apiKey: String?): Single<NewsModel>?
}