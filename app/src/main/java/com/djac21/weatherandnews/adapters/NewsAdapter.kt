package com.djac21.weatherandnews.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.djac21.weatherandnews.R
import com.djac21.weatherandnews.customTabs.CustomTabActivityHelper
import com.djac21.weatherandnews.customTabs.CustomTabActivityHelper.openCustomTab
import com.djac21.weatherandnews.customTabs.WebViewActivity
import com.djac21.weatherandnews.databinding.ItemViewNewsBinding
import com.djac21.weatherandnews.models.ArticlesItem

class NewsAdapter(private val news: List<ArticlesItem>, private val context: Context) :
    RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemViewNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val articleItem = news[position]
        holder.bind(articleItem)

        Glide.with(context)
            .load(articleItem.urlToImage)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
            )
            .into(holder.itemViewNewsBinding.image)
    }

    override fun getItemCount(): Int = news.size

    inner class ViewHolder(var itemViewNewsBinding: ItemViewNewsBinding) : RecyclerView.ViewHolder(itemViewNewsBinding.root), View.OnClickListener {
        fun bind(articlesItem: ArticlesItem) {
            itemViewNewsBinding.article = articlesItem
            itemViewNewsBinding.executePendingBindings()
        }

        override fun onClick(view: View) {
            val url = news[absoluteAdapterPosition].url

            if (validateUrl(url)) {
                val uri = Uri.parse(url)
                uri?.let { openCustomChromeTab(it) }
            } else {
                Toast.makeText(context, "Error with link", Toast.LENGTH_SHORT).show()
            }
        }

        private fun validateUrl(url: String?): Boolean {
            return url != null && url.isNotEmpty() && (url.startsWith("http://") || url.startsWith("https://"))
        }

        private fun openCustomChromeTab(uri: Uri) {
            val intentBuilder = CustomTabsIntent.Builder()
            val customTabsIntent = intentBuilder.build()

            val params = CustomTabColorSchemeParams.Builder()
                .setNavigationBarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                .setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setSecondaryToolbarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                .build()
            intentBuilder.setColorSchemeParams(CustomTabsIntent.COLOR_SCHEME_DARK, params)

            openCustomTab(context, customTabsIntent, uri, object : CustomTabActivityHelper.CustomTabFallback {
                override fun openUri(activity: Context?, uri: Uri?) {
                    openWebView(uri!!)
                }
            })
        }

        private fun openWebView(uri: Uri) {
            val webViewIntent = Intent(context, WebViewActivity::class.java)
            webViewIntent.putExtra(WebViewActivity.EXTRA_URL, uri.toString())
            webViewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(webViewIntent)
        }

        init {
            itemView.setOnClickListener(this)
        }
    }
}