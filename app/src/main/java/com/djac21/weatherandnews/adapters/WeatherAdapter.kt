package com.djac21.weatherandnews.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.djac21.weatherandnews.R
import com.djac21.weatherandnews.databinding.ItemViewWeatherBinding
import com.djac21.weatherandnews.models.ListItem
import com.djac21.weatherandnews.utils.Utils

class WeatherAdapter(private var mainWeatherList: List<ListItem>?, private val context: Context) : RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemViewWeatherBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listItem = mainWeatherList?.get(position)
        listItem?.let { holder.bind(it) }

        Glide.with(context)
            .load(Utils.imageURL(listItem?.weather?.get(0)?.icon))
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
            )
            .into(holder.itemViewWeatherBinding.image)
    }

    override fun getItemCount(): Int = mainWeatherList?.size!!

    inner class ViewHolder(var itemViewWeatherBinding: ItemViewWeatherBinding) : RecyclerView.ViewHolder(itemViewWeatherBinding.root) {
        fun bind(item: ListItem) {
            itemViewWeatherBinding.listItem = item
            itemViewWeatherBinding.executePendingBindings()
        }
    }
}