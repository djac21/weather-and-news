package com.djac21.weatherandnews.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import java.text.SimpleDateFormat
import java.util.*

object Utils {
    fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val networkCapabilities = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                else -> false
            }
        } else {
            return connectivityManager.activeNetworkInfo?.isConnected ?: false
        }
    }

    fun imageURL(iconCode: String?): String {
        return "https://openweathermap.org/img/wn/${iconCode}@2x.png"
    }

    fun maxMinFormat(max: Double?, min: Double?): String {
        return "Max: ${degreeFormat(max)} | Min: ${degreeFormat(min)}"
    }

    fun degreeFormat(number: Double?): String {
        return "%.2f \u2109".format(number)
    }

    fun unixConverter(unixInt: Int): String {
        val unix = unixInt * 1000L
        val dateFormat = SimpleDateFormat("MM/dd", Locale.US)
        val dt = Date(unix)
        return dateFormat.format(dt)
    }

    fun nullCheck(inputString: String?): String {
        return inputString ?: "N/A"
    }
}