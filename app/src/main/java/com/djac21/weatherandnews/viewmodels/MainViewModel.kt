package com.djac21.weatherandnews.viewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import com.djac21.weatherandnews.api.Repo

class MainViewModel : ViewModel() {
    fun getWeather(context: Context, cityName: String) = Repo().getWeather(context, cityName)

    fun getForecast(context: Context, cityName: String) = Repo().getForecastWeather(context, cityName)

    fun getNews(context: Context, cityName: String) = Repo().getNews(context, cityName)
}