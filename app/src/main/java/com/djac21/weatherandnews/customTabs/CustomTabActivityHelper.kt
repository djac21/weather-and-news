package com.djac21.weatherandnews.customTabs

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent

object CustomTabActivityHelper {
    @JvmStatic
    fun openCustomTab(activity: Context, customTabsIntent: CustomTabsIntent, uri: Uri?, fallback: CustomTabFallback?) {
        val packageName = CustomTabsHelper.getPackageNameToUse(activity)

        if (packageName == null) {
            fallback?.openUri(activity, uri)
        } else {
            customTabsIntent.intent.setPackage(packageName)
            customTabsIntent.intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            customTabsIntent.launchUrl(activity, uri!!)
        }
    }

    interface CustomTabFallback {
        fun openUri(activity: Context?, uri: Uri?)
    }
}