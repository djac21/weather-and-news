package com.djac21.weatherandnews.customTabs

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.djac21.weatherandnews.R

class ChromeTabActionBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val data = intent.dataString
        if (data != null)
            Toast.makeText(context, getToastText(context, intent.getIntExtra(KEY_ACTION_SOURCE, -1)), Toast.LENGTH_SHORT).show()
    }

    private fun getToastText(context: Context, actionSource: Int): String {
        return when (actionSource) {
            ACTION_MENU_ITEM_1 -> context.getString(R.string.action_back)
            ACTION_MENU_ITEM_2 -> context.getString(R.string.action_forward)
            ACTION_ACTION_BUTTON -> context.getString(R.string.action_bookmark)
            else -> context.getString(R.string.action_settings)
        }
    }

    companion object {
        const val KEY_ACTION_SOURCE = "org.chromium.customtabsdemos.ACTION_SOURCE"
        const val ACTION_MENU_ITEM_1 = 1
        const val ACTION_MENU_ITEM_2 = 2
        const val ACTION_ACTION_BUTTON = 3
    }
}