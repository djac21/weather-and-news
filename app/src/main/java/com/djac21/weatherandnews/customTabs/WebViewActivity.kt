package com.djac21.weatherandnews.customTabs

import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.djac21.weatherandnews.R

class WebViewActivity : AppCompatActivity() {
    private var progressLoading: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        val url = intent.getStringExtra(EXTRA_URL)
        val webView = findViewById<WebView>(R.id.web_view)
        progressLoading = findViewById(R.id.progress_loading)
        webView.webViewClient = WebViewClient()
        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                setPageLoadProgress(newProgress)
                super.onProgressChanged(view, newProgress)
            }
        }

        url?.let { webView.loadUrl(it) }
        title = url
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun setPageLoadProgress(progress: Int) {
        progressLoading!!.progress = progress
    }

    private fun showProgress() {
        progressLoading!!.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        progressLoading!!.visibility = View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    private inner class WebViewClient : android.webkit.WebViewClient() {
        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
            showProgress()
            super.onPageStarted(view, url, favicon)
        }

        override fun onPageFinished(view: WebView, url: String) {
            hideProgress()
            super.onPageFinished(view, url)
        }
    }

    companion object {
        const val EXTRA_URL = "extra.url"
    }
}