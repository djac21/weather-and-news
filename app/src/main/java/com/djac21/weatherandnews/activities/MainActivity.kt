package com.djac21.weatherandnews.activities

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.djac21.weatherandnews.R
import com.djac21.weatherandnews.adapters.NewsAdapter
import com.djac21.weatherandnews.adapters.WeatherAdapter
import com.djac21.weatherandnews.databinding.ActivityMainBinding
import com.djac21.weatherandnews.models.AllData
import com.djac21.weatherandnews.models.NewsModel
import com.djac21.weatherandnews.models.WeatherForecastModel
import com.djac21.weatherandnews.models.WeatherModel
import com.djac21.weatherandnews.utils.Utils
import com.djac21.weatherandnews.utils.Utils.isNetworkAvailable
import com.djac21.weatherandnews.viewmodels.MainViewModel
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {
    private val TAG = MainActivity::class.java.simpleName
    private var cityName = "New York City"
    private lateinit var activityMainBinding: ActivityMainBinding
    private lateinit var mainViewModel: MainViewModel
    private lateinit var compositeDisposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        activityMainBinding.cityName.text = cityName
        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        getWeatherAndNews()
    }

    override fun onStop() {
        compositeDisposable.clear()
        super.onStop()
    }

    private fun getWeatherAndNews() {
        if (!isNetworkAvailable(this))
            Snackbar.make(activityMainBinding.constraintLayout, "Offline mode - No internet available", Snackbar.LENGTH_SHORT).show()

        val weather = mainViewModel.getWeather(this, cityName)
        val forecast = mainViewModel.getForecast(this, cityName)
        val news = mainViewModel.getNews(this, cityName)

        activityMainBinding.progressBar.visibility = View.VISIBLE

        compositeDisposable = CompositeDisposable()
        compositeDisposable.add(
            Single.zip(weather, forecast, news, { weatherModel, weatherForecast, newsModel ->
                AllData(weatherModel, weatherForecast, newsModel)
            })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    activityMainBinding.progressBar.visibility = View.GONE
                    setCurrentWeather(it.weatherModel)
                    setForecastWeather(it.weatherForecastModel)
                    setNews(it.newsModel)
                }, { e ->
                    Log.e(TAG, "getWeatherAndNews: $e")
                    activityMainBinding.progressBar.visibility = View.GONE
                    activityMainBinding.cardView.visibility = View.GONE
                    activityMainBinding.errorMessage.visibility = View.VISIBLE
                    Snackbar.make(activityMainBinding.constraintLayout, "Error loading data", Snackbar.LENGTH_SHORT).show()
                })

        )
    }

    private fun setCurrentWeather(weatherModel: WeatherModel) {
        activityMainBinding.currentWeather = weatherModel.main
        activityMainBinding.cityName.text = cityName

        Glide.with(this)
            .load(Utils.imageURL(weatherModel.weather?.get(0)?.icon))
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
            )
            .into(activityMainBinding.weatherImage)
    }

    private fun setForecastWeather(weatherForecastModel: WeatherForecastModel) {
        activityMainBinding.cardView.visibility = View.VISIBLE
        activityMainBinding.cityName.text = cityName

        val filteredWeatherList = weatherForecastModel.list?.filterIndexed { index, _ ->
            index % 8 == 0 && index != 0
        }

        val weatherAdapter = WeatherAdapter(filteredWeatherList, this)
        activityMainBinding.recyclerViewWeather.adapter = weatherAdapter
        activityMainBinding.recyclerViewWeather.layoutManager = GridLayoutManager(this, 3)
    }

    private fun setNews(newsModel: NewsModel) {
        val newsAdapter = newsModel.articles?.let { NewsAdapter(it, this) }
        activityMainBinding.recyclerViewNews.adapter = newsAdapter
        activityMainBinding.recyclerViewNews.layoutManager = LinearLayoutManager(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchItem = menu.findItem(R.id.searchView)
        val searchView = searchItem.actionView as SearchView
        searchView.queryHint = getString(R.string.search_in_your_city)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                cityName = query.trim()
                getWeatherAndNews()
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                return false
            }
        })
        return true
    }
}